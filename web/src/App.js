import React, { Component } from 'react';
import Header from './components/Header'
import Footer from './components/Footer'
import Homepage from './pages/Homepage'
import About from './pages/About'
import Contact from './pages/Contact'
import Services from './pages/Services'
import Careers from './pages/Careers'
import Governance from './pages/Governance'
import Fostering from './pages/Fostering'
import './App.css';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
class App extends Component {
  render() {
    return (
          <Router>
              <div className="wrapper">
                  <Header/>
                  <Switch>
                      <Route exact path="/" component={Homepage}/>
                      <Route exact path="/about" component={About}/>
                      <Route exact path="/services" component={Services}/>
                      <Route exact path="/fostering" component={Fostering}/>
                      <Route exact path="/careers" component={Careers}/>
                      <Route exact path="/contact" component={Contact}/>
                      <Route exact path="/governance" component={Governance}/>
                      <Redirect to="/"/>

                  </Switch>
                  <Footer/>
              </div>
          </Router>
   );
  }
}

export default App;
