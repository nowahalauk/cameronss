export const EMAIL_ADDRESS = 'info@harvestcaregroup.org.uk';
export const ADDRESSES = {
  BISCOT: {
      address: '73A Biscot Road, Luton, Beds, LU3 1AH',
      phone_number: '01582524091'
  }
};
