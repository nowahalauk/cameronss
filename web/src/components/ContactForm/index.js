import React from 'react';

const ContactForm = () => {
    return (
        <div className="xs-contact-form-wraper">
            <h4>Drop us a line</h4>
            <form action="#" method="POST" id="xs-contact-form" className="xs-contact-form contact-form-v2">
                <div className="input-group">
                    <input type="text" name="name" id="xs-name" className="form-control" placeholder="Enter Your Name....."/>
                    <div className="input-group-append">
                        <div className="input-group-text"><i className="fa fa-user"></i></div>
                    </div>
                </div>
                <div className="input-group">
                    <input type="email" name="email" id="xs-email" className="form-control" placeholder="Enter Your Email....." />
                    <div className="input-group-append">
                        <div className="input-group-text"><i className="fa fa-envelope-o"></i></div>
                    </div>
                </div>
                <div className="input-group massage-group">
                    <textarea name="massage" placeholder="Enter Your Message....." id="xs-massage" className="form-control" cols="30" rows="10"></textarea>
                    <div className="input-group-append">
                        <div className="input-group-text"><i className="fa fa-pencil"></i></div>
                    </div>
                </div>
                <button className="btn btn-success" type="submit" id="xs-submit">submit</button>
            </form>
        </div>
    )
};

export default ContactForm;