import React from 'react';

const TeamProfile = ({ img, name, role, borderColor }) => (
    <div className="xs-single-team">
        <img src={img} alt={name} />
        <div className="xs-team-content">
            <h4>{name}</h4>
            <small>{role}</small>
            <svg className="xs-svgs" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 270 138">
                <path className={`fill-${borderColor}`} d="M375,3294H645v128a10,10,0,0,1-10,10l-250-20a10,10,0,0,1-10-10V3294Z" transform="translate(-375 -3294)"/>
            </svg>
        </div>
    </div>
);

export default TeamProfile;