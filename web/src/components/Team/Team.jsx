import React from 'react';
import craigCameron from '../../images/team/craig_cameron.jpg'
import cliveEllington from '../../images/team/clive_ellington.jpg'
import arleneTaylor from '../../images/team/arlene_taylor.jpg'

import TeamProfile from "./TeamProfile";
const Team = () => (
    <section className="xs-section-padding bg-gray">
        <div className="container">
            <div className="xs-heading row xs-mb-60">
                <div className="col-md-9 col-xl-9">
                    <h2 className="xs-title">Our Team</h2>
                    <span className="xs-separetor dashed"></span>
                    <p>Harvest Care Group are keen to recruit committed, earnest individuals who have a heart for children and young people to join the company.   The Company is in the process of opening new services such as Residential Care Home and Fostering.  We are keen to get the right people for the right job.</p>
                </div>
            </div>
            <div className="row xs-mb-60">
                <div className="col-md-6 col-lg-4">
                    <TeamProfile
                        name="Craig Cameron"
                        role="CEO & Founder"
                        borderColor="red"
                        img={craigCameron}
                    />
                </div>
                <div className="col-md-6 col-lg-4">
                    <TeamProfile
                        name="Clive Ellington"
                        role="Scheme Manager"
                        borderColor="red"
                        img={cliveEllington}
                    />
                </div>
                <div className="col-md-6 col-lg-4">
                    <TeamProfile
                        name="Arlene Taylor"
                        role="Operations Manager"
                        borderColor="red"
                        img={arleneTaylor}
                    />
                </div>
            </div>
        </div>
    </section>
);

export default Team;