import React from 'react';
import ServicePromo from '../ServicePromo/ServicePromo';
const WhatWeDo = () => {
    return (
        <section className="xs-content-section-padding">
            <div className="container">
                <div className="xs-heading row col-lg-10 xs-mb-70 text-center mx-auto">
                    <h2 className="xs-mb-0 xs-title">Our values <span className="color-green">represent the way we</span> work with people around us.</h2>
                </div>
                <div className="row">
                    <ServicePromo
                        title="Semi Independent living"
                        description="Semi - Independent living can be a positive choice and experience an important stage
                        in their lives. This can be achieved by helping young people to face up to difficulties and find ways of dealing with them."
                    />
                    <ServicePromo
                        title="Fostering"
                        description="We work hard to make a good match between the child and foster parent - we want positive role models, parenting skills and warmth so that the young person has every possible chance of reaching their potential."
                    />
                    <ServicePromo
                        title="Protection for our young people"
                        description="We have a responsibility to make sure that children and young people in our care are
                        protected from bullying, abuse and neglect."
                    />
                    <ServicePromo
                        title="Supported Accommodation"
                        description="The support we provide forms part of a range of services, which work
                        closely together to meet young people’s needs, and support families and carers."
                    />
                </div>
            </div>
        </section>
    )
};

export default WhatWeDo;