import React from 'react';
import PropTypes from 'prop-types';
const ServicePromo = ({ title, description, icon = '', color = 'orange'}) => {
    return (
        <div className="col-md-6 col-lg-3">
            <div className="xs-service-promo">
                <span className={`${icon} color-${color}`}></span>
                <h5>{title}</h5>
                <p>{description}</p>
            </div>
        </div>
    )
};

ServicePromo.propTypes = {
  title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
  ]),
    description: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
  ]),
    icon: PropTypes.string,
    color: PropTypes.string
};

export default ServicePromo;