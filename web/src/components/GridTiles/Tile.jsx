import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
const Tile = ({title, description, url ='', image}) => {
    return (
        <div className="col-lg-4 col-md-6">
            <div className="xs-popular-item xs-box-shadow">
                {image && <div className="xs-item-header">
                    <img src={image} alt={title}/>
                </div>}
                <div className="xs-item-content">

                    <span className="xs-post-title xs-mb-30">{title}</span>

                    <p>{description}</p>

                    <span className="xs-separetor"></span>

                    <div className="row xs-margin-0">
                        <div className="xs-avatar-title">
                            {url && <Link to={url}>Find out more</Link>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

Tile.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    url: PropTypes.string,
};

export default Tile;