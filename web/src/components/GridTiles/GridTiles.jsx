import React from 'react';
import Tile from './Tile';
const GridTiles = () => {
    return (
        <section className="bg-gray waypoint-tigger xs-section-padding xs-popularCauses-v2">
            <div className="container">
                <div className="xs-heading row xs-mb-60">
                    <div className="col-md-9 col-xl-9">
                        <h2 className="xs-title">Our Homes</h2>
                        <span className="xs-separetor dashed"></span>
                        <p>Harvest Care Group semi-independent units are situated primarily in the London regions and Bedfordshire.</p>
                    </div>
                </div>
                <div className="row">
                    <Tile title="Wembley" description="172 Monks Park Wembley, Middlesex HA9 6LA"/>
                    <Tile title="Croydon" description="121 Waddon Park Avenue, Croydon CR0 4LX"/>
                    <Tile title="Luton" description="73B Biscott Road Luton, Bedfordshire LU3"/>
                </div>
            </div>
        </section>
    );
};
export default GridTiles;
