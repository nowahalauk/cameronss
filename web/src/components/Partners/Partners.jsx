import React from 'react';
import PropTypes from 'prop-types';
import barnetCouncil from '../../images/partner/barnet-logo-green.png'
import brentCouncil from '../../images/partner/brent-council.png'
import haringeyCouncil from '../../images/partner/haringey-council.jpg'
import wandsworthCouncil from '../../images/partner/wandsworth-council.png'

const Partners = ({ backgroundImage }) => {
    return (
        <section className="bg-gray xs-partner-section"
                 style={{
                     backgroundImage: `url(${backgroundImage})`
                 }}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-5">
                        <div className="xs-partner-content">
                            <div className="xs-heading xs-mb-40">
                                <h2 className="xs-mb-0 xs-title">We work with <span className="color-green">many.</span></h2>
                            </div>
                            <p>Harvest Care Group has established good working relationships with a number of Local Authorities across London, Bedfordshire and Hertfordshire.  We are a growing and developing company whose specialises in the care and support of vulnerable children and young people, by providing accommodation with support and outreach.</p>
                            <a href="/careers" className="btn btn-primary bg-orange">
                                join us now
                            </a>
                        </div>
                    </div>
                    <div className="col-lg-7">
                        <ul className="fundpress-partners">
                            <li><a href="https://www.barnet.gov.uk/" target="_blank" rel="noopener noreferrer"><img src={barnetCouncil} alt="Barnet Council" /></a></li>
                            <li><a href="https://www.brent.gov.uk/" target="_blank" rel="noopener noreferrer"><img src={brentCouncil} alt="Brent Council" /></a></li>
                            <li><a href="http://www.haringey.gov.uk/" target="_blank" rel="noopener noreferrer"><img src={haringeyCouncil} alt="Haringey Council" /></a></li>
                            <li><a href="http://www.wandsworth.gov.uk/" target="_blank" rel="noopener noreferrer"><img src={wandsworthCouncil} alt="Wandsworth Council" /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
)
};

Partners.propTypes = {
    backgroundImage: PropTypes.string
};

export default Partners;