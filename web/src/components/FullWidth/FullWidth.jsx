import React from 'react';
import PropTypes from 'prop-types';
const FullWidth = ({ backgroundImage }) => {
    return (
        <div className="xs-section-paddding xs-ipad-overlay xs-funFact-v3 waypoint-tigger"
             style={{
                 backgroundImage: `url(${backgroundImage})`
             }}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-5"></div>
                    <div className="col-lg-7">
                        <div className="xs-funfact-content-wraper">
                            <div className="xs-heading">
                                <h2 className="xs-title">Empowering Young Lives.</h2>
                                <p>When a young people comes into one of our units, a key worker is allocated to the young person who will dedicate their time by giving the young person support and will walk side by side through the young persons development.  Each young person has a care plan and independent programme that they will work with to develop life skills.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

FullWidth.propTypes = {
    backgroundImage: PropTypes.string
};

export default FullWidth;