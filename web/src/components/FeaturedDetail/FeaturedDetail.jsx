import React from 'react';
import featuredImage1 from '../../images/feature-image-1.jpg'
import featuredImage2 from '../../images/feature-image-2.jpg'
const FeaturedDetail = () => {
    return (
        <section className="xs-section-padding">
            <div className="container">
                <div className="row">
                    <div className="col-lg-7">
                        <div className="xs-feature-text-content">
                            <div className="xs-heading">
                                <h3 className="xs-title" data-title="Harvest">Who We Are</h3>
                                <h2>Our business is people and people are our business</h2>
                                <span className="xs-separetor bg-bondiBlue"></span>
                            </div>
                            <p>Harvest Care Group has successfully been working with young people providing semi-independent accommodation with support and outreach between the ages of 16yrs to 18yrs since 2014. A developing provider of care and support for children and young people.</p>
                            <a href="/about" className="btn btn-secondary bg-bondiBlue">Read More</a>
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <div className="xs-feature-image-box image-1">
                            <img src={featuredImage1} alt="Featured "/>
                        </div>
                        <div className="xs-feature-image-box image-2">
                            <img src={featuredImage2} alt="Featured 2"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default FeaturedDetail;