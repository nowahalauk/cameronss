import React from 'react';
import Logo from '../../images/footer_logo_white.png';
import {ADDRESSES, EMAIL_ADDRESS} from "../../constants/index";
const Footer = () => {
    return(
        <div>
            <footer className="xs-footer-section">
                <div className="container">
                    <div className="xs-footer-top-layer">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 footer-widget xs-pr-20">
                                <a href="index.html" className="xs-footer-logo">
                                    <img src={Logo} alt="Logo" />
                                </a>
                                <p>Harvest Care Group has successfully been working with young people providing semi-independent accommodation with support and outreach between the ages of 16yrs to 18yrs since 2014.</p>
                                <ul className="xs-social-list-v2">
                                    <li><a href="" className="color-facebook"><i className="fa fa-facebook"></i></a></li>
                                    <li><a href="" className="color-twitter"><i className="fa fa-twitter"></i></a></li>
                                    <li><a href="" className="color-pinterest"><i className="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>


                            <div className="col-lg-4 col-md-6 footer-widget">
                                <h3 className="widget-title">Contact Us</h3>
                                <ul className="xs-info-list">
                                    <li><i className="fa fa-home bg-red"></i>{ADDRESSES.BISCOT.address}</li>
                                    <li><i className="fa fa-phone bg-green"></i>{ADDRESSES.BISCOT.phone_number}</li>
                                    <li><i className="fa fa-envelope-o bg-blue"></i><a href={`mailto:${EMAIL_ADDRESS}`}>{EMAIL_ADDRESS}</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-4 col-md-6 footer-widget">
                                <h3 className="widget-title">Pages</h3>
                                <ul className="xs-footer-list">
                                    <li><a href="/about">About</a></li>
                                    <li><a href="/services">Services</a></li>
                                    <li><a href="/careers">Careers</a></li>
                                    <li><a href="/contact">Contact</a></li>
                                    <li><a href="/governance">Governance</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="xs-copyright">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="xs-copyright-text">
                                    <p>&copy; Copyright {new Date().getFullYear()} Harvest Care Group</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="xs-back-to-top-wraper">
                    <a href="#" className="xs-back-to-top"><i className="fa fa-angle-up"></i></a>
                </div>
            </footer>
        </div>
    )
};

export default Footer;