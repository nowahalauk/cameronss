import React from 'react';
import { Link } from "react-router-dom";

const Navigation = () => {
    return (
        <ul className="nav-menu">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/services">Services</Link></li>
            <li><Link to="/fostering">fostering</Link></li>
            <li><Link to="/careers">Careers</Link></li>
            <li><Link to="/contact">Contact</Link></li>
        </ul>
    )
};

export default Navigation;
