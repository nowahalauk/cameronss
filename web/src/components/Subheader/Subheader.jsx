import React from 'react';
import PropTypes from 'prop-types';

const Subheader = ({ title, subtitle, backgroundImage}) => {
    return (
        <section className="xs-banner-inner-section parallax-window"
                 style={{
                     backgroundImage: `url(${backgroundImage})`
                 }}>
            <div className="xs-black-overlay"></div>
            <div className="container">
                <div className="color-white xs-inner-banner-content">
                    <h2>{title}</h2>
                    <p>{subtitle}</p>
                    <ul className="xs-breadcumb">
                        <li className="badge badge-pill badge-primary"><a href="/" className="color-white"> Home /</a> {title}</li>
                    </ul>
                </div>
            </div>
        </section>
    )
};

Subheader.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string
};

export default Subheader;