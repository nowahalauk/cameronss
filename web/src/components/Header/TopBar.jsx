import React from 'react';
import {Link} from 'react-router-dom';
import {EMAIL_ADDRESS} from "../../constants";
const TopBar = () => {
    return (
        <div className="xs-top-bar clearfix">
            <div className="container">
                <Link to={`mailto:${EMAIL_ADDRESS}`} className="xs-top-bar-mail"><i className="fa fa-envelope-o"></i>{EMAIL_ADDRESS}</Link>
            </div>
        </div>
    )
};

export default TopBar;