import React from 'react';
import Navigation from '../Navigation/Navigation';
import TopBar from './TopBar';
import Logo from '../../images/logo.svg';
import {Link} from 'react-router-dom';

const Header = () => {
    return(
        <div>
            <header className="header xs-header xs-fullWidth">
                <TopBar />
                <div className="container">
                    <nav className="xs-menus">
                        <div className="nav-header">
                            <div className="nav-toggle"></div>
                            <a href="/" className="xs-nav-logo">
                                <img src={Logo} alt="Logo" />
                            </a>
                        </div>
                        <div className="nav-menus-wrapper row">
                            <div className="xs-logo-wraper col-lg-3">
                                <a href="/" className="nav-brand">
                                    <img src={Logo} alt="Logo"/>
                                </a>
                            </div>
                            <div className="col-lg-6 nav-container">
                                <Navigation />
                            </div>
                            <div className="xs-navs-button d-flex-center-end col-lg-3">
                                <a href="/contact" className="btn btn-primary">
                                    <span className="badge"><i className="fa fa-heart"></i></span> Get In Touch
                                </a>
                            </div>
                        </div>
                    </nav>

                </div>
            </header>
        </div>
    )
};

const HeaderV2 = () => {
    return (
        <header className="xs-header header-transparent xs-box">
            <div className="container">
                <nav className="xs-menus">
                    <div className="xs-top-bar clearfix">
                        <ul className="xs-top-social">
                            <li><Link to="/"><i className="fa fa-facebook"></i></Link></li>
                            <li><Link to="/"><i className="fa fa-twitter"></i></Link></li>
                            <li><Link to="/"><i className="fa fa-instagram"></i></Link></li>
                        </ul>
                        <Link to="mailto:info@harvestcargroup.org.uk" className="xs-top-bar-mail"><i className="fa fa-envelope-o"></i>info@harvestcargroup.org.uk</Link>
                    </div>
                    <div className="nav-header">
                        <div className="nav-toggle"></div>
                        <Link to="/" className="xs-nav-logo">
                            <img src={Logo} alt="Harvest Care Group Logo"/>
                        </Link>
                    </div>
                    <div className="nav-menus-wrapper row">
                        <div className="xs-logo-wraper col-lg-2 col-xl-2 xs-padding-0">
                            <Link className="nav-brand" to="/">
                                <img src={Logo} alt="arvest Care Group Logo"/>
                            </Link>
                        </div>
                        <div className="col-lg-10 col-xl-7">
                            <Navigation />
                        </div>
                        <div className="xs-navs-button col-lg-3 col-xl-3 d-block d-lg-none d-xl-block">
                            <Link to="/" className="btn btn-primary">
                                <span className="badge"><i className="fa fa-heart"></i></span> Get In Touch
                            </Link>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
    )
};

export default Header;