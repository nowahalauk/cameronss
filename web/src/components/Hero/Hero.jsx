import React from 'react';
import PropTypes from 'prop-types';
const Hero = ({ children }) => {
    return (
        <section className="xs-welcome-slider">
            <div className="xs-banner-slider owl-carousel">
                {children}
            </div>
        </section>
    )
};

Hero.propTypes = {
    backgroundImage: PropTypes.string
};
export default Hero;