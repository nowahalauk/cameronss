import React from 'react';
import PropTypes from 'prop-types';
const HeroImage = ({ backgroundImage = '', title = 'title', subtitle = 'subtitle', ctaButtons = [] }) => {
    return (
        <div className="xs-welcome-content"
             style={{
                 backgroundImage: `url(${backgroundImage})`
             }}>

            <div className="container row">
                <div className="xs-welcome-wraper banner-verion-2 color-white col-md-8 content-left">
                    <p>{subtitle}</p>
                    <h2>{title}</h2>
                    <div className="xs-btn-wraper">
                        {ctaButtons.map(({text}) => {
                            return (
                                <a href="#" className="btn btn-outline-primary">
                                    {text}
                                </a>
                            )
                        })}
                        <a href="/careers" className="btn btn-outline-primary">
                            join us now
                        </a>
                        <a href="/about" className="btn btn-primary">
                            <span className="badge"><i className="fa fa-chevron-right"></i></span> Read More
                        </a>
                    </div>
                </div>
            </div>
            <div className="xs-black-overlay"></div>
        </div>
    )
};

HeroImage.propTypes = {
    backgroundImage: PropTypes.string,
    title: PropTypes.string,
    subtitle: PropTypes.string
};
export default HeroImage;