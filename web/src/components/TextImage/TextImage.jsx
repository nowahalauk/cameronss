import React from 'react';
const TextImage = () => {
    return (
        <section className="xs-section-padding">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-lg-6">
                        <div className="xs-text-content xs-pr-20">
                            <h2 className="color-navy-blue">Semi-independent accommodation</h2>
                            <p>Young people are supported in all areas of their health and wellbeing through access to education, employment to develop living skills, to build positive relationships, to manage their allowances and to build resilience to manage their life productively.</p>
                            <blockquote>
                                If you don't understand how fast and easy it is to so long for your favorite charity on FundPress, please try it. <span>How it works</span> page, <span>Contact us</span>.
                            </blockquote>
                            <a href="#" className="btn btn-primary bg-light-red">
                                <span className="badge"><i className="fa fa-chevron-right"></i></span> Read More
                            </a>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="xs-feature-image">
                            <img src="assets/images/features_1.jpg" alt="" />
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="xs-feature-image">
                            <img src="assets/images/features_2.jpg" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
};

export default TextImage;