import React from 'react';
import subHeaderImage from '../../images/backgrounds/contact_bg.jpg';
import Subheader from "../../components/Subheader/Subheader";
import ContactForm from '../../components/ContactForm';
const Contact = () => {
    return(
        <div>
            <Subheader
                title="Contact"
                subtitle="Its all about children and young people"
                backgroundImage={subHeaderImage}
            />
            <main className="xs-main">
                <div className="xs-contact-section-v2">
                    <section className="">
                        <div className="container">
                            <div className="xs-contact-container">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <ContactForm/>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="xs-maps-wraper map-wraper-v2">
                                            <div id="xs-map" className="xs-box-shadow-2">
                                                <iframe title="Monks Park" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2480.7984185676264!2d-0.2702263843052634!3d51.5535952796427!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876119cf9f60f99%3A0x2fbfc3b17c3f4f55!2s172+Monks+Park%2C+Wembley!5e0!3m2!1sen!2suk!4v1530128874390" width="100%" height="100%" frameBorder="0" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="xs-contact-details">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 col-lg-4">
                                    <div className="xs-contact-details">
                                        <div className="xs-widnow-wraper">
                                            <div className="xs-window-top">
                                                <img src="assets/images/contact/contact-info-img-1.png" alt=""/>
                                            </div>
                                            <div className="xs-window-back">
                                                <iframe title="Biscot Road" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2462.5130737049067!2d-0.4287995842159795!3d51.88810027969951!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48764856ec17d1bb%3A0xadb7f1bda12eab3!2s73B+Biscot+Rd%2C+Luton+LU3+1AH!5e0!3m2!1sen!2suk!4v1530128597985" width="100%" height="100%" frameBorder="0" allowfullscreen></iframe>
                                            </div>
                                            <div className="xs-window-nav">
                                                <a href="#" className="xs-window-opener">
                                                    <i className="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <ul className="xs-unorder-list">
                                            <li><i className="fa fa-phone color-green"></i>0208 900 8645</li>
                                            <li><i className="fa fa-envelope-o color-green"></i>info@harvestcaregroup.org.uk</li>
                                            <li><i className="fa fa-map-marker color-green"></i>Wembely</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-4">
                                    <div className="xs-contact-details">
                                        <div className="xs-widnow-wraper">
                                            <div className="xs-window-top">
                                                <img src="assets/images/contact/contact-info-img-2.png" alt=""/>
                                            </div>
                                            <div className="xs-window-back">
                                                <iframe title="Biscot Road" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2462.5130737049067!2d-0.4287995842159795!3d51.88810027969951!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48764856ec17d1bb%3A0xadb7f1bda12eab3!2s73B+Biscot+Rd%2C+Luton+LU3+1AH!5e0!3m2!1sen!2suk!4v1530128597985" width="100%" height="100%" frameBorder="0" allowfullscreen></iframe>
                                            </div>
                                            <div className="xs-window-nav">
                                                <a href="#" className="xs-window-opener">
                                                    <i className="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <ul className="xs-unorder-list">
                                            <li><i className="fa fa-phone color-green"></i>0158 252 4091</li>
                                            <li><i className="fa fa-envelope-o color-green"></i>info@harvestcaregroup.org.uk</li>
                                            <li><i className="fa fa-map-marker color-green"></i>Luton</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-4">
                                    <div className="xs-contact-details">
                                        <div className="xs-widnow-wraper">
                                            <div className="xs-window-top">
                                                <img src="assets/images/contact/contact-info-img-3.png" alt=""/>
                                            </div>
                                            <div className="xs-window-back">
                                                <iframe title="Waddon Park" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2490.900606968983!2d-0.12032468431235403!3d51.36812387961239!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876073fe1b79135%3A0x86172b98b0beb608!2s121+Waddon+Park+Ave%2C+Croydon!5e0!3m2!1sen!2suk!4v1530128804851" width="100%" height="100%" frameBorder="0" allowfullscreen></iframe>
                                            </div>
                                            <div className="xs-window-nav">
                                                <a href="#" className="xs-window-opener">
                                                    <i className="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <ul className="xs-unorder-list">
                                            <li><i className="fa fa-phone color-green"></i>0208 837 4928</li>
                                            <li><i className="fa fa-envelope-o color-green"></i>info@harvestcaregroup.org.uk</li>
                                            <li><i className="fa fa-map-marker color-green"></i>Croydon</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </main>
        </div>
    )
};

export default Contact;