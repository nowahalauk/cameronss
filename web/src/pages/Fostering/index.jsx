import React from 'react';
import subHeaderImage from '../../images/backgrounds/fostering.png';
import Subheader from "../../components/Subheader/Subheader";
import ContactForm from "../../components/ContactForm";

import clive from '../../images/team/clive_ellington.jpg'

const Fostering = () => {
    return(
        <div>
            <Subheader
                title="Harvest Care Group Fostering"
                subtitle="Providing a safe space for young people to live healthy and positive lives"
                backgroundImage={subHeaderImage}
            />
            <section className="xs-content-section-padding">
                <div className="container">
                    <div className="xs-heading row col-lg-10 xs-mb-70 text-center mx-auto">
                        <h2 className="xs-mb-0 xs-title">Get <span className="color-green">Involved</span></h2>
                        <br/>
                        <p>Providing a safe space for young people to live healthy and positive lives.</p>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="xs-service-promo">

                                <h5>Become a foster parent</h5>
                                <p>Fostering is one of the most varied, challenging and rewarding jobs available.</p>
                                <p>We are always recruiting more foster carers, particularly for teenagers, disabled children, young asylum seekers and siblings.</p>
                                <p>We actively welcome foster parents from all backgrounds.</p>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="xs-service-promo">

                                <h5>Join Our Review Panel</h5>
                                <p>Our fostering panel plays a crucial role in the provision and monitoring of foster care for children and young people. They make decisions on approving family matches, the terms of approval and assess the continuing ability of foster parents to meet the children's needs. These are key tasks focused on ensuring the child's welfare.</p>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="xs-service-promo">

                                <h5>Support And Training</h5>
                                <p>We support the roles of foster parent and panel member in a variety of ways; through training, payment and ongoing support from experts.</p>
                                <p>If you are interested in joining us please do get in touch today. We have children waiting who need our support.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="xs-section-padding bg-gray">
                <div className="container">
                    <div className="xs-heading row xs-mb-60">
                        <div className="col-md-6 col-xl-6">
                            <div className="xs-feature-image-box image-1">
                                <img src={clive} alt="Clive Ellington"/>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-6">
                            <h2 className="xs-title">About Us</h2>
                            <span className="xs-separetor dashed"></span>
                            <p>Harvest Care Fostering is lead by Clive Ellington and forms part of Harvest Care Group. Harvest Care Group has successfully been working with young people providing semi-independent accommodation with support and outreach between the ages of 16yrs to 18yrs since 2014.</p>
                            <span className="xs-separetor dashed"></span>
                            <a href="/careers" className="btn btn-primary">Meet The Team</a>
                        </div>

                    </div>
                </div>
            </section>
            <section className="">
                <div className="container">
                    <div className="xs-contact-container">
                        <div className="row">
                            <div className="col-md-12">
                                <ContactForm/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default Fostering;