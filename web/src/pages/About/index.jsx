import React from 'react';
import Subheader from "../../components/Subheader/Subheader";
import subHeaderImage from '../../images/backgrounds/about_bg.jpg';
import subHeaderImage2 from '../../images/backgrounds/parallax_1.jpg';

const About = () => {
    return(
        <div>
           <Subheader
               title="About Us"
               subtitle="our business is people, and people is our business"
               backgroundImage={subHeaderImage}
           />

            <section className="xs-content-section-padding">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 content-center">
                            <div className="xs-heading xs-mb-100 text-center">
                                <h2 className="xs-mb-0 xs-title">Providing a safe space for <span className="color-green">young people</span> to live healthy and positive lifes.</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row xs-mb-100">
                        <div className="col-md-6">
                            <div className="xs-about-feature">
                                <p className="lead">We at Harvest Care Group are proud that 90% of our young people enter into ETE. With our strong partnerships with ETE providers and agencies as well as our focused key work sessions we have been able to support 9 out 0f 10 young people we work with to gain employment or enter into induction and/or training.</p>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="xs-about-feature">
                                <p className="lead">Our staff assess each individual's needs and work with them to put in place a plan to meet their aspirations and to prepare them for independent living. We will work with other agencies and stakeholders when required and take into account any views of families or guardians if appropriate. them for independent living.</p>
                            </div>
                        </div>

                    </div>
                    <div className="row">
                        <div className=" col-md-4">
                            <div className="xs-about-feature">
                                <h3>Our Vision</h3>
                                <p className="lead">To give every child and young person the opportunity to reach their fullest potential and to find the uniqueness and right to belong.</p>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="xs-about-feature">
                                <h3>Our Values</h3>
                                <ul className="xs-unorder-list play green-icon">
                                    <li>Promote Value</li>
                                    <li>Provide Unconditional Regard</li>
                                    <li>Personal service</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <div className="xs-funfact-section xs-content-section-padding waypoint-tigger parallax-window" style={{
                backgroundImage: `url(${subHeaderImage2})`
            }}>
                <div className="container">
                    <div className="row col-lg-10 xs-heading mx-auto">
                        <h2 className="xs-title color-white small">Our staff care for children and young people.</h2>
                    </div>
                </div>
                <div className="xs-black-overlay"></div>
            </div>
            <section className="xs-content-section-padding">
                <div className="container">
                    <div className="xs-heading row col-lg-10 xs-mb-70 text-center mx-auto">
                        <h2 className="xs-mb-0 xs-title">Success <span className="color-green">Story</span></h2>
                    </div>
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="xs-feature-image-box image-1">
                            </div>
                        </div>
                        <div className="col-sm-8">
                            <div className="xs-heading">
                                <h3 className="xs-title" data-title="Fulfilled">Aleem Egunjobi</h3>
                            </div>
                            <p>Hi my name is Aleem Egunjobi and I'm going to share a short story about my life.
                                I was in the care system from I was a teenager. Being in the care system was difficult period of my life
                                were I was put into care and separated from my family. From this period I started to rebel, I started smoking
                                cannabis and hanging around with the wrong people which had influenced me into doing wrong things,such
                                as selling drugs which wasted my time instead of trying to get an education.
                            </p>
                                <p>After getting into trouble with the law, I was allocated a mentor - Craig Cameron, who supported and advised me in the areas where
                                other people were unable to support me with such as getting an education/training and into employment
                                through Harvest Care Group (formerly Cameron Support Services).</p>
                            <p>At a young age I always believed in God but I didn't have many people around me with the same belief and
                                faith until I met my mentor and introduced me to a church that he attended and from there on I started to go to
                                church.</p>
                            <p>
                                With the support I received my life has been never been the same again, I am happy, fulfilled and living a
                                life of faith and achievement through Harvest.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default About;