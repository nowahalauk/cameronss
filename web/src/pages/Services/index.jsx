import React from 'react';
import subHeaderImage from '../../images/backgrounds/services_bg.jpg';
import Subheader from "../../components/Subheader/Subheader";
import Partners from "../../components/Partners/Partners";
import WhatWeDo from "../../components/WhatWeDo/WhatWeDo";

const Services = () => {
    return(
        <div>
            <Subheader
                title="Services"
                subtitle="Helping to protect the next generation"
                backgroundImage={subHeaderImage}
            />
            <WhatWeDo />
            <Partners/>
        </div>
    )
};

export default Services;