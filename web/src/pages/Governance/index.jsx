import React from 'react';
import Subheader from "../../components/Subheader/Subheader";
import subHeaderImage from '../../images/backgrounds/governance_bg.jpg';
import employeePrivacyNotice from '../../assets/Employee_Privacy_Notice_Policy.pdf';
import residentPrivacyNotice from '../../assets/Resident_Privacy_Notice.pdf';
import staffRecruitmentPolicy from '../../assets/Staff_Recruitment_Policy.pdf';
import safeGuardingPolicy from '../../assets/Safeguarding_Policy_140518.pdf';
import jobApplicantPrivacy from '../../assets/Job_Applicant_Privacy_Notice.pdf';
const slaveryAndHumanTrafficking = 'https://firebasestorage.googleapis.com/v0/b/harvest-care-group.appspot.com/o/docs%2FSlavery_and_Human_Trafficking_Statement_Oct_18.pdf?alt=media&token=544fed43-7b2f-43de-b331-479c8c28793a';
const Governance = () => {
    return(
        <div>
            <Subheader
                title="Governance"
                backgroundImage={subHeaderImage}
            />
            <section className="xs-content-section-padding">
                <div className="container">
                    <div className="row col-md-12 mx-auto">
                        <div className="col-lg-3">
                            <ul className="nav flex-column xs-nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <li className="nav-item">
                                    <a className="nav-link active show" data-toggle="pill" href="#privacynotices">Privacy Notices</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" data-toggle="pill" href="#policies">Policies</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-lg-9">
                            <div className="tab-content" id="v-pills-tabContent">
                                <div className="tab-pane slideUp active" id="privacynotices" role="tabpanel">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="xs-tab-content">
                                                <h5>Resident Information Privacy</h5>
                                                <p>This notice sets out the types of information (data) we hold about you, how we use that information
                                                    and how long we keep it for.</p>
                                                <a href={residentPrivacyNotice} target="_blank">Download Document</a>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="xs-tab-content">
                                                <h5>Employee Privacy Notice</h5>
                                                <p>The Company is aware of its obligations under the General Data Protection Regulation (GDPR) and is
                                                    committed to processing your data securely and transparently.</p>
                                                <a href={employeePrivacyNotice} target="_blank">Download Document</a>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="xs-tab-content">
                                                <h5>Job Applicant Privacy Notice (GDPR compliant)</h5>
                                                <p>The Company is aware of its obligations under the General Data Protection Regulation (GDPR) and is
                                                    committed to processing your data securely and transparently.</p>
                                                <a href={jobApplicantPrivacy} target="_blank">Download Document</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane slideUp" id="policies" role="tabpanel">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="xs-tab-content">
                                                <h5>Safeguarding Policy</h5>
                                                <p>Harvest Care Group (HCG) acknowledges the duty of care to safeguard and promote the
                                                    welfare of children and is committed to ensuring safeguarding practice reflects statutory
                                                    responsibilities, government guidance and complies with best practice and Ofsted regulatory
                                                    requirements.</p>
                                                <a href={safeGuardingPolicy} target="_blank">Download Document</a>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="xs-tab-content">
                                                <h5>Safer Staff Recruitment & Selection Policy</h5>
                                                <p>It is the intention of Harvest Care Group to ensure that the most skilled and experienced
                                                    individuals are attracted to the vacancies, in order to continue to provide high quality support
                                                    services to our children and young people.</p>
                                                <a href={staffRecruitmentPolicy} target="_blank">Download Document</a>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="xs-tab-content">
                                                <h5>Slavery and Human Trafficking Statement</h5>
                                                <p>Modern slavery is a term used to encompass slavery, servitude, forced and compulsory labour, bonded and child labour and human trafficking..</p>
                                                <a href={slaveryAndHumanTrafficking} target="_blank">Download Document</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default Governance;