import React from 'react';
import { Hero, HeroImage } from '../../components/Hero';
import FeaturedDetail from '../../components/FeaturedDetail/FeaturedDetail';
import GridTiles from '../../components/GridTiles/GridTiles';
import FullWidth from '../../components/FullWidth/FullWidth';
import WhatWeDo from '../../components/WhatWeDo/WhatWeDo';
import Partners from '../../components/Partners/Partners';

import sliderImage from '../../images/slider/homepage_image.jpg'
import funFactImage from '../../images/backgrounds/funfact-bg-image.jpg';

const Homepage = () => {
    return(
        <div>
            <Hero>
                <HeroImage subtitle="Semi-independent Accommodation & Outreach Support" title="Harvest Care Group" backgroundImage={sliderImage}/>
            </Hero>
            <FeaturedDetail/>
            <GridTiles/>
            <FullWidth backgroundImage={funFactImage}/>
            <WhatWeDo />
            <Partners/>
        </div>
    )
};

export default Homepage;