import React from 'react';
import Subheader from "../../components/Subheader/Subheader";
import Team from "../../components/Team/Team";
import subHeaderImage from '../../images/backgrounds/career_bg.jpg';
import craigCameron from '../../images/team/craig_cameron.jpg'
import applicationForm from '../../assets/HCG_Application_Form_Pack.pdf';
const Careers = () => {
    return(
        <div>
           <Subheader
               title="Careers"
               subtitle="Give a helping hand for young people"
               backgroundImage={subHeaderImage}
           />
            <section className="xs-content-section-padding">
                <div className="container">
                    <div className="xs-heading row col-lg-10 xs-mb-70 text-center mx-auto"><h3 className="xs-mb-0 xs-title">Why Work For Harvest Care Group</h3></div>

                    <p>Our team find working for Harvest Care Group to be highly rewarding. They enjoy seeing our young people thrive, they know our pay and benefits are competitive, and they feel their ideas and feedback are valued. We hold regular team days, we actively promote training and development, and we respect each and every person who works with us.</p>
                    <p>We are committed to safeguarding children and young people. Our interviews will always include relevant questions relating to child protection and / or safeguarding. The successful applicant will be required to undertake vetting checks including an enhanced Disclosure and Barring Service (DBS) check.</p>
                </div>
            </section>
            <Team/>
            <section className="xs-content-section-padding">
                <div className="container">
                    <div className="xs-heading row col-lg-10 xs-mb-70 text-center mx-auto">
                        <h2 className="xs-mb-0 xs-title">Words from <span className="color-green">CEO/Founder</span></h2>
                    </div>
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="xs-feature-image-box image-1">
                                <img src={craigCameron} alt="Craig Cameron"/>
                            </div>
                        </div>
                        <div className="col-sm-8">
                            <div className="xs-heading">
                                <h3 className="xs-title" data-title="promising">Craig Cameron</h3>
                            </div>
                            <p>Harvest Care Group (formerly Cameron Support Services) was born out of a heart and desire to
                                young people who are experiencing a number of personal and social issues which have impacted and
                                affected them from fulfilling their potential.
                            </p>
                            <p>Having been brought up and lived in the London for most of my life, I have witnessed the issues that
                                have affected young people such as poverty, identity issues, involvement in gangs and drugs culture
                                for which many ended up in the youth offending services. A group of us young men started
                                mentoring service for young people to help, support and empower young people to lead positive
                                lifestyles, access education, employment and training and to find their identity and belonging.</p>
                            <p>This service evolved and developed into providing accommodation with support to young people,
                                we found many who either were homeless/care leavers. To date we have supported many young
                                people developing their life skills, resilience and building positive relationships moving in to
                                independent accommodation. For some young people who move into independence we offer
                                outreach as a means of helping them settle into their new environment.</p>
                            <p>
                                The vision for the future is to expand and develop the services for children and young people, our
                                hope for the future is to open a children’s home and fostering services.
                            </p>
                            <p>The future looks promising...</p>
                        </div>
                    </div>

                </div>
            </section>
            <section className="xs-section-padding bg-gray">
                <div className="container">
                    <div className="xs-heading row xs-mb-60">
                        <div className="col-md-9 col-xl-9">
                            <h2 className="xs-title">Recruitment</h2>
                            <span className="xs-separetor dashed"></span>
                        </div>
                    </div>
                    <div className="row col-md-12 mx-auto">
                        <div className="col-lg-3">
                            <ul className="nav flex-column xs-nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <li className="nav-item">
                                    <a className="nav-link active show" data-toggle="pill" href="#application">Application</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" data-toggle="pill" href="#vacancies">Vacancies</a>
                                </li>

                            </ul>
                        </div>
                        <div className="col-lg-9">
                            <div className="tab-content" id="v-pills-tabContent">
                                <div className="tab-pane slideUp active" id="application" role="tabpanel">
                                    <div className="row">
                                        <div className="col-md-9">
                                            <div className="">
                                                <h5>Application Process</h5>
                                                <p>Top apply for a vacancy with us at Harvest Care Group vacancy, simply complete the application form pack and we will bbe in touch with you.</p>
                                                <a href={applicationForm} target="_blank">Download Application Form</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane slideUp" id="vacancies" role="tabpanel">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="xs-tab-content">
                                                <h5>No Vacancies</h5>
                                                <p>we currently have no vacancies at the moment, but please keep checking.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    )
};

export default Careers;