import React from 'react';
import Logo from '../../images/logo.svg'
const ComingSoon = () => {
    return(
        <div>
            <img src={Logo} alt="logo"/>
            <h1>We're Coming soon</h1>
            <p>Contact us: <a href="mailto:info@harvestcaregroup.org.uk">info@harvestcaregroup.org.uk</a> </p>
        </div>
    )
};

export default ComingSoon;